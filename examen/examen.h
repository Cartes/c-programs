#ifndef __EXAMEN_H_
#define __EXAMEN_H_

#include <stdlib.h>
#include <stdio.h>

#define MAX 0x100
#define N 0x100
struct TPos {
    double x;
    double y;
};

struct TEstrella {
    struct TPos pos;
    double distancia;
    double diametro;
    double masa;
    int edad;
    int brillo;
};

struct TPila {
    struct TEstrella * data[N];
    int cima;
};

#ifndef __cplusplus
extern "C" {
#endif

    int menu ();
    void push (struct TPila *p, struct TEstrella *nuevo);
    void Pregunta (struct TPila *pila, struct TEstrella *almacenado, FILE *pf);

#ifndef __cplusplus
}
#endif

#endif
