#ifndef __SNAKE_H__
#define __SNAKE_H__


#include <stdlib.h>
#include <string.h>
#include <ncurses.h>

#define AMAX 0x200
struct TVector {
    double x;
    double y;
};


struct TAnillo {
    struct TVector pos;
    struct TVector vel;
};


struct TSnake {
    struct TAnillo anillo [AMAX];
    int cima;
    int vidas;

};

#ifdef __cplusplus
extern "C" {
#endif

    void iniciar       ( int lines, int cols   );
    void parir         ( struct TSnake *snake  ); // Da una posicion Inicial
    void mover         ( struct TSnake *snake );
    void crecer        ( struct TSnake *snake );
    void movimiento    ( struct TAnillo *cabeza, int tecla);



#ifdef __cplusplus
}
#endif


#endif

//tabe
