#include <stdio.h>
#include <stdlib.h>

#define X 0x100
struct EmpleadoN {
    char nombre[X];
    int edad;
    double sueldo;
};
int main(int argc, char *argv[]) {

    struct EmpleadoN normal;

    printf("Dime tu nombre empleado: ");
    scanf(" %s", normal.nombre);

    printf("Dime tu edad: ");
    scanf (" %i", &normal.edad);

    printf("Dime tu sueldo: ");
    scanf(" %lf", &normal.sueldo);

    printf("Tu nombre es: %s\n\n Tu edad es: %i\n\n tu sueldo es: %.2lf\n\n", normal.nombre, normal.edad, normal.sueldo);

    return EXIT_SUCCESS;
}
