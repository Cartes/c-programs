#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada
#define N 16
int main(int argc, char *argv[]) {

    double f, E = 1.0;

    for (int i=1; i<=N; i++){
        f = 1;
        for (int j=1; j<=i; j++){
            f = f * j; // DE AQUI SACAMOS EL FACTORIAL
            printf (" %i", j);
        }
        E = E + 1.0/f;
    }

    printf ("Valor de E es %.16lf\n", E);

    return EXIT_SUCCESS;
}
