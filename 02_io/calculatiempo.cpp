#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada

int main() {
    int edad, dia;

    printf("Dime tu edad: ");
    scanf(" %d", &edad);

    dia = edad * 365;

    printf("Has vivido %d\n", dia, edad);

    return EXIT_SUCCESS;
}
