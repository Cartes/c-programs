#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <ncurses.h>
//Con split abrimos mas terminales, con C+W nos movemos
#include "snake.h"

void pintar (struct TSnake snake){

    clear();
    for (int i=0; i<snake.cima; i++)
    mvprintw (snake.anillo[i].pos.y, snake.anillo[i].pos.x, "o"); // Es como un prinw solo que hay que darle la x y.
    refresh (); //Volcamos el array a la pantalla
}
int main(int argc, char *argv[]) {

    struct TSnake snake;
    int input;
    bool end = false;
    initscr (); // Hace una matri inicial, carga lines la cantidad de filas y cols la cantidad de columnas
    halfdelay (2);
    keypad (stdscr, TRUE);
    curs_set (0);
    iniciar (LINES, COLS);
    parir (&snake);

    do {
        input = getch ();
        movimiento (&snake.anillo[0], input);
        mover (&snake);
        if (snake.anillo[0].pos.x > COLS)
            snake.anillo[0].pos.x = 0;
        if (snake.anillo[0].pos.y > LINES)
            snake.anillo[0].pos.y = 0;
        if (snake.anillo[0].pos.x < 0)
            snake.anillo[0].pos.x = COLS;
        if (snake.anillo[0].pos.y < 0)
            snake.anillo[0].pos.y = LINES;
            pintar (snake);
    } while (input != 0x1B && !end);

    curs_set (1);

    endwin ();


    return EXIT_SUCCESS;
}

/*cpv jasm bmp*/
