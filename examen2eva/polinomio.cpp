#include <math.h>
#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada
#define MAX 10
#define INC 0.5

double polinomio(double c[MAX], double x/*Parametros*/) {
    double f = 0,
           potencia; //solo se definen en esta funcion

      for (int termino=0; termino<MAX; termino++){
        potencia  = 1;
          for (int exponente=0; exponente<termino; exponente++)
          potencia *= x;
        f += c[termino] * potencia;
    }
      return f; // valor de retorno
}

int main() {
    double f = 0,  ultimo = 0,x;
    double t;

    double c[MAX] = {3,1, 0, 0, 0, 0, 0, 0, 0, 0};


    printf ("Limite inferior : ");
    scanf (" %lf", &x);
 /*   1 x^ + 2 x^1 + 3 x^2 + 4 x^3 5 x^4
    c[i]*x^i
    f += c[i]*x^i la i es termino
 */
    for (t=x; t == x || ultimo*f>0 ;t+=INC){
        ultimo = f;
        f = polinomio(c, t);
    if (t == x)
        ultimo = f;
    printf ("f(%.2lf) = %.2lf\n", t, f);
    getchar();
  }

    printf("Signo cambia en %.2lf", t);

/*funcion trozo de codigo que contiene algo y solo hace 1 cosa
**tiene 1 nombre
**puede recibir parametros
**puede tener un valor de retorno
**puede tener 1 o varias llamadas
**que cuando le pasamos los parametros los podemos pasar o bien por valor o bien por referencia
*/
    return EXIT_SUCCESS;
}
