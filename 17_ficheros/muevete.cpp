#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define FILENAME "aburr.txt"

int main(int argc, char *argv[]) {
    FILE *pf;
    int pos;

    srand (time (NULL));
    if ( !(pf = fopen (FILENAME, "r")) ){
        fprintf(stderr, "Ain.\n");
        return EXIT_FAILURE;
    }
    pos = rand () % 100; // Consigo numero aleatorios

    fseek (pf, pos, SEEK_SET); // Nos permite mover el cursor de un lado a otro SEEK_SET DESDE EL PRINCIPIO DEL PRINCIPIO SEEK_CUR DESDE EL MEDIO SEEK_END DESDE EL FINAL
    printf ("Pos: %i => %c\n", pos, (char) getc (pf));

    fclose(pf);

    return EXIT_SUCCESS;
}
