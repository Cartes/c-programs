#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada
#define N 25
#define A 30
int main() {
    int div[N];
    int div1[A];
    int num = 25, n_div=1;
    int num1 = 30, n_div1=1;

    for (int pd=num/2; pd>1; pd--)
        if (num % pd == 0)
            div[n_div++] = pd;
    for (int i=0; i<n_div; i++)
        printf("%i ", div[i]);

    printf("\n");

    for (int pd1=num1/2; pd1>1; pd1--)
        if (num1 % pd1 == 0)
            div1[n_div1++] = pd1;
    for (int i=0; i<n_div1; i++)
        printf("%i ", div1[i]);

    printf("\n");

    return EXIT_SUCCESS;
}
