#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada
double fc (int p, int b){
    if (p==1)
        return b;
    return  b + 1. / fc (b,p-1);
}

int main(int argc, char *argv[]) {

    int p, b;

    printf ("BASE: ");
    scanf (" %i", &b);
    printf ("Profundidad: ");
    scanf (" %i", &p);

    fc (b,p);

    printf (" %.2lf", fc(b,p));

    return EXIT_SUCCESS;
}
