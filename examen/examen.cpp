#include <stdio.h>
#include <stdlib.h>

#include "examen.h"

#define volcado "estrellas.txt"

int main(int argc, char *argv[]) {

    struct TEstrella *estrella;
    FILE *pf;
    struct TPila *p;

    if ( !(pf = fopen(volcado, "w")) ){
        fprintf(stderr, "No se ha podido abrir");
        return EXIT_FAILURE;
    }


    Pregunta(p, estrella, pf);

    fclose(pf);


    return EXIT_SUCCESS;
}
