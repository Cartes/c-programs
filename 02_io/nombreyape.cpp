#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada

int main(){
    char nombre[32];
    char apellido1[32];
    char apellido2[32];
    printf("Dime tu nombre; ");
    scanf(" %s", nombre);
    printf("Dime tus apellidos; ");
    scanf(" %s %s", apellido1, apellido2);
    printf("Hola, %s %s %s\n", nombre, apellido1, apellido2);

    return EXIT_SUCCESS;
}
