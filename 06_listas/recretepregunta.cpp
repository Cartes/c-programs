#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada

int main() {
    char respuesta;

    do {
    printf("Quieres que te cuente un cuento\n"
            "Recuento que nunca se acaba (s/n)");
    scanf(" %c", &respuesta);
    if (respuesta != 'n')
        printf("Yo no te digo ni que si ni que no,\n"
                "lo que digo es que si.\n ");
    } while ( respuesta != 'n' );

    printf("Como no hay que ser un pesado en esta vida: ADIOS. Isabel.\n");

    return EXIT_SUCCESS;
}
