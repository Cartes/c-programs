#include <stdio.h>
#include <stdlib.h>


#define N 2
enum Opcion {
    suma, // es lo mismo que vale 0
    resta, // es lo mismo que vale 1 y asi sucesivamente
    multiplicacion,
    division,
    OPCIONES
};

const char *texto[] = {
    "suma",
    "resta",
    "multiplcacion",
    "division"
};

const char simb[] = "+-x/";

double sum  (double op1, double op2){return op1 + op2;}
double res  (double op1, double op2){return op1 - op2;}
double mult (double op1, double op2){return op1 * op2;}
double div  (double op1, double op2){return op1 / op2;}

void titul () {
    system ("clear");
    system ("toilet -fpagga CALCULADORA");
    printf("\n");
}

enum Opcion menu () {
    int opcion;
    do {
    system ("clear");
    system ("toilet -fpagga CALCULADORA");
    printf("\n");
    printf("\nOpciones:");
    printf("\n=========\n\n");
    for (int i=0; i<OPCIONES; i++)
        printf("\t %i.- %s\n", i+1, texto[i]);
    printf("\nOpciones: ");

    scanf (" %i", &opcion);
    opcion--;
    }while(opcion<suma || opcion  >= OPCIONES);

    return (enum Opcion) opcion;
};

void pide_op (double ops[N]) {
    titul();
    for (int i=0; i<N; i++) {
        printf("Operando %i: ", i+1);
        scanf(" %lf", &ops[i]);
    }
}

void ver_op (double op[N]) {
    titul();
    for ( int i=0; i<N; i++)
        printf ("\t%lf\n", op[i]);
}
int main(int argc, char *argv[]) {

    enum Opcion op;
    double operando[N];
    double (*operaciones []) (double, double) = {
        &sum, &res, &mult, &div, NULL
    };

    op = menu();
    pide_op (operando);
    titul();

    printf("%s: (%lf %c %lf) = %lf\n\n", texto[op], operando[0], simb[op], operando[1], (*operaciones[op]) (operando[0], operando[1]));

    return EXIT_SUCCESS;
}

//hacer lo mismo pero con una cadena de texto una que lo haga inverso y otra cambie la o por las i
