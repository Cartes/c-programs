#include <stdio.h>
#include <stdlib.h>


#define A 12
#define FIBO "fibonacci.dat"

int main(int argc, char *argv[]) {

    unsigned int matriz[A] = {1,1,2,3,5,8,13,21,34,55,89,144};
    int n = sizeof (matriz) / sizeof (int);

    FILE *pf = NULL;

    if (!(pf = fopen (FIBO, "wb"))){
        fprintf(stderr, "Puede que tengas un fallo\n");
          return EXIT_FAILURE;
    }

        fwrite(matriz,sizeof(int), n,pf);

    ftell(pf);
    printf("Ocupa: %i", (char) getc (pf));

    fclose(pf);
    return EXIT_SUCCESS;
}
