#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada

int main() {

    int i,j,n;

    printf("Ingresa un numero; ");
    scanf("%i", &n);
    printf("\n");

    /* quita espacios en blanco */
    for (i=1; i<=n; i++){
        for(j=1; j<=n; j++)
        printf(" ");
    /* Saca el numero de filas indicado */
        for (j=1; j<=i; j++)
            printf("*");

        printf("\n");

    }

    return EXIT_SUCCESS;
}
