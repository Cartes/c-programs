#include <stdio.h>
#include <stdlib.h>

#define EDITOR "guardado.txt"

int main(int argc, char *argv[]) {

    char *guardado;
    FILE *dem;
    char e;

    if ( !(dem = fopen (guardado, "r")) )
        return EXIT_FAILURE;

    while ( (e = getc (dem)) != EOF)
        printf ("%c", e);
    fclose (dem);
    return EXIT_SUCCESS;
}
