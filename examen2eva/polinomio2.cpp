#include <math.h>
#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada
#define MAX 10
#define INC 0.5

int main() {
    double f = 0, potencia, ultimo = 0,x;

    double c[MAX] = {3,1, 0, 0, 0, 0, 0, 0, 0, 0};


    printf ("Limite inferior : ");
    scanf (" %lf", &x);
 /*   1 x^ + 2 x^1 + 3 x^2 + 4 x^3 5 x^4
    c[i]*x^i
    f += c[i]*x^i la i es termino
 */
    for (double t=x; t == x || ultimo*f>0 ;t+=INC){
        ultimo = f;
        f = 0;
      for (int termino=0; termino<MAX; termino++){
        potencia  = 1;
          for (int exponente=0; exponente<termino; exponente++)
          potencia *= t;
        f += c[termino] * potencia;
    }
    if (t == x)
        ultimo = f;
    printf ("f(%.2lf) = %.2lf\n", t, f);
    getchar();
  }
/*
    for (int vez=0; vez<potencia; vez++)
        resultado *= x;
*/
    return EXIT_SUCCESS;
}
