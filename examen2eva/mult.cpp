#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada
#define M 2
#define K 4
#define N 3
int main() {

    int i, j;
    double A[M][K] = {
                      {2, 3, 5, 1},
                      {3, 1, 4, 2}
                     },
           B[K][N] = {
                      {5, 2, 1},
                      {3, -7, 2},
                      {-4, 5, 1},
                      {2, 3, -9}
                     },
           C[M][N];
/*
    printf ("Fila:");
    scanf (" %i", &i);
    printf ("Columna:");
    scanf (" %i", &j);
*/
    for (int i=0; i<M; i++)
        for(int j=0; j<N; j++){
          C[i][j] = 0;
          for (int k=0; k<K; k++)
            C[i][j] += A[i][k] * B[k][j];
        }

   /* printf ("C[%i][%i] = %.2lf\n", C[i][j]);*/
    for (int i=0; i<N; i++){
      for (int j=0; j<N; j++)
          printf("\t%2.f\n", C[i][j]);

          /* printf ("A[%2.f, %2.f, %2.f]\n", C[0][0], C[0][1], C[0][2]);*/
          /* printf ("B[%2.f, %2.f, %2.f]\n", C[1][j], C[1][j], C[1][j]);*/
    }

    return EXIT_SUCCESS;
}
