#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

void pon_titulo(int tabla){ /* PARAMETRO FORMAL */
    char titulo[MAX];
    sprintf(titulo, "toilet -fpagga --gay Tabla del %i", tabla);
    system(titulo);
}

//Funcion punto de entrada

int main() {
    /*DECLARACIÓN DE VARIABLES */
    int tabla, op1 = 1;

    /*MENU*/
    printf("¿Que tabla quieres?\n ");
    printf("Tabla: ");
    scanf(" %i", &tabla);

    /*TITULO*/
    pon_titulo(5); /* LLAMADA CON PARAMETRO ACTUAL 5 */
    pon_titulo(tabla);
    pon_titulo(2*tabla);

    /*RESULTADO*/
    printf("1x%i=%i\n", tabla, op1 * tabla);
    op1++;
    printf("2x%i=%i\n", tabla, op1 * tabla);

    return EXIT_SUCCESS;
}
