#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada
#define M 2
#define K 4
#define N 3

//solo existe la variable cuando solo se ejecuta en esta parte. //la N te dice con cada fila te dice cuantas columnas se tiene que saltar por eso quitamos la M

void imprime (double *m, int f, int c, const char *titulo){
    printf("\n");
    printf("%s:\n", titulo);
    for (int i=0; i<f; i++){
        for (int j=0; j<c; j++)
            printf("\t%7.2lf", *(m + i * c +j));
       printf("\n");
    }
}
int main() {

    int i, j;
    double A[M][K] = {
                      {2, 3, 5, 1},
                      {3, 1, 4, 2}
                     },
           B[K][N] = {
                      {5, 2, 1},
                      {3, -7, 2},
                      {-4, 5, 1},
                      {2, 3, -9}
                     },
           C[M][N];
    for (int i=0; i<M; i++)
        for(int j=0; j<N; j++){
          C[i][j] = 0;
          for (int k=0; k<K; k++)
            C[i][j] += A[i][k] * B[k][j];
        }
    imprime ((double *) A, M, K, "Matriz A");
    imprime ((double *) B, K, N, "Matriz B");
    imprime ((double *) C, M, N, "Matriz C (A+B)");



    return EXIT_SUCCESS;
}
