#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada
#define N 15
#define D 5

int main() {

    int emento[N];
    double emento2[D];

    /*CONDICIONES DE CONTORNO*/
    emento[1] = emento[0] = 1;
    emento2[1] = emento2 [0] = 1;

    // CALCULOS
    for (int i=2; i<N; i++)
         emento[i] = emento[i-1] + emento[i-2];


    // SALIDA DE DATOS
    for (int i=0; i<N; i++)
       printf(" %i", emento[i]);

    printf("\n");

    for (int i=1; i<N; i++)
        printf(" %lf", (double) emento[i] / emento[i-1]);

    printf("\n\n\n\n");

       return EXIT_SUCCESS;
}
