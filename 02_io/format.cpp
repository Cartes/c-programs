#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada

int main() {

    printf("%c", 97);
    printf("%c", 0x61);
    printf("%c", 'a');

    puts("");
    printf("%i", 97);
    /*
    4 => '4'
    4 + '0'
    4 + 0x30

    scanf("%i");
    '4'
    '4' - '0'=> 4
    '7'
    4*10 => 40
    '7' - '0' => 7
    40 + 7 => 47
    '5'
    47 * 10 => 470
    '5' - '0' => 5
    470 + 5
    475
*/
    puts("");

    printf("%i\n%i\n%i\n%i\n%i\n%i\n%i\n%i\n%i\n%i\n%i\n%i\n%i\n%i\n%i\n%i\n", 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);
    printf("\thola\n");

    int numero;
    printf("Num: ");
    scanf(" %i", &numero);
    printf("Numero => [%p]: %i\n",&numero, numero); /* donde esta el %p va a meter el &numero el aspersan */
    printf("Linea %i\n", 47);

    int dia, annio;
    printf("iNacimiento dd/mm/aaaa: ");
    scanf(" %i %*i %i", &dia, &annio); /* Caracter de supresión de asignación el asterisco */

    int hex[32];
    scanf(" %[0-9a-fA-F]", hex); /* Conjunto de selección */
    return EXIT_SUCCESS;
}
