#include <stdio.h>
#include <stdlib.h>


#define NOMBRE "cancion.txt"

/*contiene la direccion del salto de linea*/
const char * song = "\n\
                Believe for every drop of rain that falls\n\
                A flower grows\n\
                I believe that somewhere in the darkest night\n\
                A candle glows\n\
\n\
                I believe for everyone who goes astray, someone will come\n\
                To show the way\n\
                I believe, I believe\n\
\n\
                I believe above a storm the smallest prayer\n\
                Can still be heard\n\
                I believe that someone in the great somewhere\n\
                Hears every word\n\
\n\
                Everytime I hear a new born baby cry,\n\
                Or touch a leaf or see the sky\n\
                Then I know why, I believe\n\
\n\
                Everytime I hear a new born baby cry,\n\
                Or touch a leaf or see the sky\n\
                Then I know why, I believe\n\
\n\
      ";

void print_usage () {
    printf ("Esto se usa asi\n");
}

void informo (const char *msg){
    print_usage ();
    fprintf(stderr, "%s\n", msg);
        exit (1);
}

int main(int argc, char *argv[]) {

    FILE *fichero;

    if ( !(fichero = fopen ( NOMBRE, "w" ))) //Crea un tubo
      informo ("No se ha podido abrir el fichero");
    fprintf (fichero, "%s", song);
    getchar ();
    fclose (fichero); //Con esto cerramos el tubo

    return EXIT_SUCCESS;
}
