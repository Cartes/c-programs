#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada
#define ROJO 4
#define AMAR 2
#define AZUL 1

int main() {

    char respuesta;
    int color = 0;

    /*Bloque de entrada de datos*/
    printf("¿Ves rojo? (s/n): ");
    scanf(" %c", &respuesta);
       if (respuesta  == 's')
        color |= ROJO;

    printf("Ves amarillo? (s/n): ");
    scanf(" %c", &respuesta);
       if (respuesta == 's')
        color |= AMAR;

    printf("Ves azul (s/n): ");
    scanf(" %c", &respuesta);
       if (respuesta == 's')
        color |= AZUL;

    switch(color)  {
        case 0:
          printf("Negro.\n");
          break;
        case 1:
          printf("Azul.\n");
          break;
        case 2:
          printf("Amarillo.\n");
          break;
        case 3:
          printf("Verde.\n");
          break;
        case 4:
          printf("Rojo.\n");
          break;
        case 5:
          printf("Morado.\n");
          break;
        case 6:
          printf("Naranja.\n");
          break;
        case 7:
          printf("Blanco.\n");
          break;

    }
    return EXIT_SUCCESS;
}
