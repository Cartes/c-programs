#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define FILENAME "aburr.txt"

int main(int argc, char *argv[]) {
    FILE *pf;
    int pos;

    if ( !(pf = fopen (FILENAME, "r")) ){
        fprintf(stderr, "Ain.\n");
        return EXIT_FAILURE;
    }

    ftell (pf);
    printf ("Ocupa: %i", (char) getc (pf));

    fclose(pf);

    return EXIT_SUCCESS;
}
