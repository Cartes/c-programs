#include <stdio.h>
#include <stdlib.h>
#define H 1.0
#define A 10

float f (int n){
    if (n == 0)
        return 1;

    else
        return n * f (n-1);
}
int main(int argc, char *argv[]) {

    int n=0;
    float E=0;

    for (n=0; n<A; n++)
    {
        E = E + (H / f(n));
        printf ("%.8lf", E);
    }

    printf ("%.8lf", E);

    return EXIT_SUCCESS;
}
