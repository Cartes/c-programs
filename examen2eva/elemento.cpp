#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada
#define M 2
#define K 4
#define N 3
int main() {

    int i, j;
    double A[M][K] = {
                      {2, 3, 5, 1},
                      {3, 1, 4, 2}
                     },
           B[K][N] = {
                      {5, 2, 1},
                      {3, -7, 2},
                      {-4, 5, 1},
                      {2, 3, -9}
                     },
           C[M][N];

   //  Preguntar al usuario la fila y la columna
    printf ("Fila:");
    scanf (" %i", &i);
    printf ("Columna:");
    scanf (" %i", &j);

    C[i][j] = 0;
    for (int k=0; k<K; k++)
        C[i][j] += A[i][k] * B[k][j];

    printf ("C[%i][%i] = %.2lf\n", i, j, C[i][j]);


    return EXIT_SUCCESS;
}
