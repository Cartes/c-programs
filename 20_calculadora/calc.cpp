#include <stdio.h>
#include <stdlib.h>


#define N 2
enum Opcion {
    suma, // es lo mismo que vale 0
    resta, // es lo mismo que vale 1 y asi sucesivamente
    multiplicacion,
    division,
    OPCIONES
};

const char *texto[] = {
    "suma",
    "resta",
    "multiplcacion",
    "division"
};


double sum  (double op1, double op2){return op1 + op2;}
double res  (double op1, double op2){return op1 - op2;}
double mult (double op1, double op2){return op1 * op2;}
double div  (double op1, double op2){return op1 / op2;}

void titul () {
    system ("clear");
    system ("toilet -fpagga CALCULADORA");
    printf("\n");
}

enum Opcion menu () {
    int opcion;
    do {
    system ("clear");
    system ("toilet -fpagga CALCULADORA");
    printf("\n");
    printf("\nOpciones:");
    printf("\n=========\n\n");
    for (int i=0; i<OPCIONES; i++)
        printf("\t %i.- %s\n", i+1, texto[i]);
    printf("\nOpciones: ");

    scanf (" %i", &opcion);
    opcion--;
    }while(opcion<suma || opcion  >= OPCIONES);

    return (enum Opcion) opcion;
};

void pide_op (double ops[N]) {
    titul();
    for (int i=0; i<N; i++) {
        printf("Operando %i: ", i+1);
        scanf(" %lf", &ops[i]);
    }
}

void ver_op (double op[N]) {
    titul();
    for ( int i=0; i<N; i++)
        printf ("\t%lf\n", op[i]);
}
int main(int argc, char *argv[]) {

    enum Opcion op;
    double operando[N];
    double resultado;

    op = menu();
    pide_op (operando);

    switch(op) {
        case suma:
            resultado = operando[0] + operando[1];
            break;
        case resta:
            resultado = operando[0] - operando[1];
            break;
        case multiplicacion:
            resultado = operando[0] * operando[1];
            break;
        case division:
            resultado = operando[0] / operando[1];
            break;
    }

    titul();
    printf("%s (%lf, %lf) = %lf", texto[op], operando[0], operando[1], resultado);

    return EXIT_SUCCESS;
}
