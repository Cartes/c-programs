#include <stdio.h>
#include <stdlib.h>

/*Funcion punto de entrada*/

int main() {

    unsigned primo[]   = {2, 3, 5, 7, 11, 13, 17, 19, 23};
    unsigned elementos = (unsigned) sizeof(primo) / sizeof(int);
    unsigned *peeping  = primo;
    char *tom = (char *) primo; /* se mueve de 1 en 1 */
    unsigned **police = &peeping; /* contiene la direccion de la dirección de un entero*/

    printf( " PRIMO:\n"
            " ======\n"
            " Localización (%p)\n"
            " Elementos: %u [%u .. %u]\n" /*la l significa el doble de largo de lo habitual */
            " Tamaño: %lu bytes.\n\n",
            primo,
            elementos,
            primo[0], primo[elementos-1],
            sizeof(primo));

    printf( "0: %u\n", peeping[0] );
    printf( "1: %u\n", peeping[1] );
    printf( "0: %u\n", *peeping ); /* el asterisco alli donde apunta peeping*/
    printf( "1: %u\n", *(peeping+2) ); /* leemos de byte en byte 4 en 4  */
    printf( "Tamaño: %lu bytes.\n", sizeof(peeping) );
    printf( "\n" );

    /* Memory Dump - Volcado de Memoria */
    for (int i=0; i<sizeof(primo); i++) /* od -tx4 */
         printf("%02X", *(tom + i));
    printf( "\n\n" );

    printf( "Police contiene %p\n", police );
    printf( "Primo  contiene %p\n", *police );
    printf( "Primo[0] contiene %u\n", **police );

    printf( "\n\n" );

    return EXIT_SUCCESS;
}
