#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define INC 0.0001
//Evalua el valor de un polinomio de coeficiente coef.x
double pol (double *pol, int grado, double x) {
    int lon = (grado + 1) * sizeof (double);
    double *copia= (double *) malloc(lon); //las celdas que ocupa son las de sizeof
    double resultado = 0;
    memcpy (copia, pol, lon);

    for(int i=0; i<grado; i++)
        for(int celda=0; celda<=i; celda++)
          *(copia+i) *= x;


    for(int i=0; i<grado; i++)
        resultado += *(copia + i);
    free(copia);

    return resultado;
}


double integral (double li, double ls, double (*pf)(double)) // esta funcion recibe un double y devuelve un double
{
    double area = 0;
    for (double x=li; x<ls; x+=INC)
        area += INC * (*pf)(x);

    return area;

}

double e (double x) {
    return exp(-x);
}

void menu () {

    system("clear");
    system("toilet -fpagga Integrales");
    printf("\n");

}

double parabola (double x) {
    double coef[] = {1, 0, 0};
    return pol (coef, 2, x);

}
int main(int argc, char *argv[]) {

    double li, ls = 0;

    menu();

    printf("dime el limite inferior: ");
        scanf("%lf", &li);
    printf("dime el limite superior: ");
        scanf("%lf", &ls);

    printf ("El Area es %lf", integral (li,ls, &e));

    return EXIT_SUCCESS;
}



//array con numeros
//array con punteros a otras funciones
//poner en la pila 3 numeros y que haga una funcion en los ultimos 2 numeros
//
//Mirar programa calc2. parte de la funcion pide_op
