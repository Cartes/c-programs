#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada

int main() {
    int numero;
    int n1, n2;


    printf( "\n   Introduzca primer operando: "  );
    scanf( "%d", &n1 );
    printf( "\n   Introduzca segundo operando: "  );
    scanf( "%d", &n2 );
    printf( "\n   %d * %d = %d\n", n1, n2, n1 * n2  );

    return EXIT_SUCCESS;
}
