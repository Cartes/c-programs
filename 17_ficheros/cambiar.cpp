#include <stdio.h>
#include <stdlib.h>

#define CAMBIAR "cancion.txt"

int main(int argc, char *argv[]) {

    FILE *fichero;
    char c;

    fichero = fopen(CAMBIAR, "rt");

    if (fichero == NULL){
        printf("ERROR");
        return 1;
    }

    while((c=getc(fichero))!= EOF){
        if (c=='I'){
            printf ("¡");
        }
        else
        {
            printf ("%c", c);
        }
    }

    return EXIT_SUCCESS;
}
