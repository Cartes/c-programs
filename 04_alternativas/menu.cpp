#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada

#define PI 3.14
/*
 * ALEJANDRO CARTES 
 * 
 *
 *
 */
int main() {

    int opcion;
    int l1;
    int area;
    int altura;
    int base;
    int radio;

    system("toilet -fpagga --metal CALCULAR AREAS:");
    system("toilet -fpagga *ALEJANDRO*");
    printf("               \n 1- Cuadrado \n");
    printf("               \n 2- Rectangulo\n");
    printf("               \n 3- Triangulo \n");
    printf("               \n 4- Circulo.\n");
    printf("Que opcion deseas: ");
    scanf(" %d", &opcion);

    switch(opcion) {
    case 1:       /*Calcula  el area del Cuadrado*/
        printf("Ponga el lado: ");
        scanf(" %d", &l1);
        area=l1*l1;
        printf("El area es de: %d\n ", area);
        break;

    case 2:      /*Calcula el area del Rectangulo*/
        printf("Dime la base: ");
        scanf(" %d", &base);
        printf("Dime la altura: ");
        scanf(" %d", &altura);
        area=base*altura;
        printf ("El area es de: %d\n ", area);
        break;

    case 3:      /*Calcula el area del Triangulo*/
        printf("Dime la base: ");
        scanf(" %d", &base);
        printf("Dime la altura: ");
        scanf(" %d", &altura);
        area=(base*altura)/2;
        printf("El area es de: %d\n", area);
        break;

    case 4:      /*Calcula el area del Circulo*/
        printf("Dime el radio: ");
        scanf(" %d", &radio);
        area=(radio*radio)*PI;
        printf("El area es de: %d\n", area);
        break;
    }
    return EXIT_SUCCESS;
}
