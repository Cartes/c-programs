#include <stdio.h>
#include <stdlib.h>

#define MAX 0x10

void push (int numero, int p[MAX], int *cima) {
    if (*cima < MAX) {
    p[*cima++] = numero; // si metemos un numero lo asigna a esa celda
    (*cima)++;
    } else
        fprintf(stderr, "Fila llena intentado escribir un %i.\n", numero);
}

int pop (int p[MAX], int *c) {
    int resultado = p[*c-1]; // cima apunta a la primera posicion libre
    if (*c <= 0) {
        fprintf(stderr, "La pila esta vacia");
    return -677;
}
    (*c)--;

    return resultado;
}

void titulo () {
    system ("clear");
    system ("toilet -fpagga --metal CLEAR");
}

void imprimir(int pila[MAX], int cima) {
    printf ("Fila", pila);
    for (int i=0; i<cima; i++)
        printf ("\t%i\n", pila[i]);
    printf("\n");
}
int main(int argc, char *argv[]) {
    int pila[MAX];
    int cima = 0;
    bool fin;
    int nuevo;
    int d=0;

    do {
        titulo ();
        imprimir(pila, cima);
        printf("Entrada: ");
        d = scanf ("%i", &nuevo);
        push (nuevo, pila, &cima);
    }while(!fin);

    return EXIT_SUCCESS;
}

//2 pop para hacer la operacion y un push para sacar el resultado de la operacion y dejar el resultado en la pila
