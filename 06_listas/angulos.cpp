#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//Funcion punto de entrada
#define GRADOS

#ifdef GRADOS
#define K 1
const char *unidad = "º";
#else
#define K M_PI / 180
const char *unidad = "rad";
#endif


int main() {

    for (double grados=0; grados<K*360.; grados+=K*.5)
        printf("%.2lf %s| %.3lf \n", grados, unidad, cos(grados));


    return EXIT_SUCCESS;
}
