#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//Funcion punto de entrada
int main() {
    /* printf tiene una escritura bufferizada */
   /* puts("hola");
    printf("hola\n"); */
    fprintf(stderr, "\a"); /* stderr no esta bufferizado */
    usleep(10000);
    fputc('\a', stderr);
    usleep(100000);
    printf("\a\n");

    return EXIT_SUCCESS;
}
