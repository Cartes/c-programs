#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada
#define N 10

int main() {

    int n_alumnos = 0;
    double nota[N], entrada, media = 0;
    /*ENTRADA DE DATOS*/
    do {
    printf ("me dices tu nota: ");
    scanf (" %lf", &entrada);
    if (entrada >= 0)
        nota[n_alumnos++] = entrada;
    } while (entrada >= 0);
    /*CALCULO*/
    for (int i=1; i<n_alumnos; i++)
        media += nota[i];

    media /= n_alumnos;

    /*SALIDA DATOS*/
    printf ("La media es %.2lf\n", media);

    return EXIT_SUCCESS;
}
