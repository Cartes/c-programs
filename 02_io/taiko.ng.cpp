#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 1000
#define BTU   10000 /* BASIC TIME UNIT */

//Funcion punto de entrada
int main() {
    int duracion[VECES]={0, 2, 8, 2, 8, 2, 8, 2, 8, 2, 8, 4, 8, 2, 4};
    /* debe valer para repetir cosas */
    for(int i=0; i<VECES; i++) {
        usleep(duracion[i] * BTU);
        fputc('\a',stderr);
    }

    return EXIT_SUCCESS;
}
