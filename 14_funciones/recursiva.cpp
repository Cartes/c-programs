
#include <stdio.h>
#include <stdlib.h>

#define n 2 // BASE
double fc (int p, int b){
    if (p==1)
        return b;
    return b + 1. / fc (b,p-1);
}
int main(int argc, char *argv[]) {

    int p, b;
/*    double res;*/

    printf("Dime la base: ");
    scanf(" %i", &b);
    printf("Dime hasta donde quieres llegar: ");
    scanf (" %i", &p);

    fc(p, b);

    printf(" %.2lf", fc(p, b));

    return EXIT_SUCCESS;
}
