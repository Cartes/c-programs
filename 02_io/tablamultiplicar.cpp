#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada

int main() {
    int i, numero;

    printf( "\n   Introduzca un n%cmero entero: ", 163  );
    scanf( "%i", &numero  );
    printf( "\n   La tabla de multiplicar del %d es:\n", numero  );

    for ( i = 1 ; i <= 10 ; i++  )
        printf( "\n   %d * %d = %d\n", i, numero, i * numero  );
    return EXIT_SUCCESS;
}
