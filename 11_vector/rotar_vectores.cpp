#include <math.h>
#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada
#define DIM 3

#define PI 3.14

int main() {

   int A[DIM], angulo;
   double x1, x2,
          j1, j2,
          coseno, seno,
          angulo2,
          resultado1, resultado2;

    system ("clear");
    system ("figlet Rotar un angulo con cordenadas");
    system ("toilet --metal ALEJANDRO");

    printf("Cordenadas: ");
    scanf(" %i %i", &A[0], &A[1]);

    printf("Angulo que queremos rotar: ");
    scanf(" %i", &angulo);

    angulo2=(angulo * PI/180);

    seno = sin(angulo2);
    coseno = cos(angulo2);

    printf("%.2lf\n", coseno);
    printf("%.2lf\n", seno);

    x1=(A[0] * cos(angulo2));
    x2=(A[1] * sin(angulo2));

    j1=(A[0] * (-1) * sin(angulo2));
    j2=(A[1] * cos(angulo2));

   /* printf("X: %.2lf %.2lf\n", x1, x2);
    printf("Y: %.2lf %.2lf\n", j1, j2);
   */
    resultado1=x1-x2;
    resultado2=j1-j2;

    printf("X: %.2lf\n", resultado1);
    printf("Y: %.2lf\n", resultado2);




    return EXIT_SUCCESS;
}

//
