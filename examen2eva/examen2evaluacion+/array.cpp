#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Funcion punto de entrada

#define N 7

int min (int A[], int *mayor){// mayor es el valor del ultimo numero que hemos colocado
    int p_min = 0;
    while (A[p_min] <= *mayor)
        p_min++;

    for (int i=p_min + 1; i<N; i++)
        if (A[i] < A[p_min] && A[i] > *mayor)
            p_min = 1;

    *mayor = A[p_min];
    return A[p_min];
}
int main(int argc, char *argv[]) {

    int A[N] = {3, 7, 8, 9, 5, 1, 0};
    int B[N];
    int mactual = -1;

    for (int r=0; r<N; r++)
        B[r] = min(A, &mactual);

    memcpy ( A, B, sizeof (A) );
    for (int i=0; i<N; i++)
        printf ("%i", A[i]);
    printf("\n");



    return EXIT_SUCCESS;
}
